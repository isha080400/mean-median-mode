import java.util.Arrays;

public class MeanMedianModeFinder {

    public static void getMean(int[] array, int sizeOfArray) {
        double sumOfElement = 0;

        for (int i = 0; i < sizeOfArray; i++) {
            sumOfElement += array[i];
        }

        double meanOfElements = sumOfElement / sizeOfArray;
        System.out.printf("Mean: %.2f", meanOfElements);
        System.out.println();
    }

    public static void getMedian(int[] array, int sizeOfArray){
        Arrays.sort(array);
        double medianOfElements;

        if(sizeOfArray % 2 == 0){
            medianOfElements = (double)((array[sizeOfArray/2 - 1]) + array[sizeOfArray/2]) / 2;
        }
        else{
            medianOfElements = (array[(sizeOfArray - 1) / 2]);
        }

        System.out.printf("Median: %.2f", medianOfElements);
        System.out.println();

    }

    public static void getMode(int[] array, int sizeOfArray){
        Arrays.sort(array);
        int maxFrequencyOfElement = 1, elementWithMaximumFrequency = array[0] , count = 1;

        for(int i = 1; i < sizeOfArray; i++){
            if(array[i] != array[i - 1]){
                if(count > maxFrequencyOfElement){
                    maxFrequencyOfElement = count;
                    elementWithMaximumFrequency = array[i - 1];
                }
                count = 1;
            }
            else{
                count++;
            }
        }

        System.out.println("Mode: " + elementWithMaximumFrequency);
    }

}
